# Linux notes

Репозиторий для того что-бы освежить/получить знания в Linux системах.

* [Аутентификация, Авторизация, Идентификация](auth_author_ident) 
* [UID,GID](UID_GID)
* [file_permissions](file_permissions)
* [useradd_usermod](useradd_usermod)